//
//  TimePickerButton.m
//  TimePickerProject
//
//  Created by Mikhail Baynov on 17/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import "TimePickerButton.h"

@interface TimePickerButton ()
{
    CGFloat buttonWidth;
    CGFloat offset;
}

@end


@implementation TimePickerButton

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        [self setup];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}





- (void)setup
{
    [self addSubview:self.titleLabel];
    
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [[UILabel alloc]initWithFrame:self.bounds];
        _titleLabel.textColor = [UIColor blackColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.backgroundColor = [UIColor orangeColor];
        _titleLabel.font = [UIFont fontWithName:@"Heroic Condensed" size:40];
    }
    return _titleLabel;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.titleLabel.frame = frame;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}


@end
