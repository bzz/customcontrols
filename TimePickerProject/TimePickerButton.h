//
//  TimePickerButton.h
//  TimePickerProject
//
//  Created by Mikhail Baynov on 17/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TimePickerDelegate <NSObject>

- (void)timePickerButtonPressed;

@end










@interface TimePickerButton : UIView

@property (strong, nonatomic) UILabel *titleLabel;
@property (weak, nonatomic) id<TimePickerDelegate> delegate;



@end
