//
//  ViewController.h
//  TimePickerProject
//
//  Created by Mikhail Baynov on 17/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TimePicker.h"


@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet TimePicker *timePicker;

@property (strong, nonatomic)  TimePicker *timePicker2;


@end

