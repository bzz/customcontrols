//
//  TimePicker.m
//  TimePickerProject
//
//  Created by Mikhail Baynov on 17/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import "TimePicker.h"



@interface TimePicker () <UIScrollViewDelegate> //<TimePickerDelegate>
{
    CGFloat buttonWidth;
    CGFloat offset;
}

@end


@implementation TimePicker

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self setup];
    }
    return self;
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.frame = frame;
        [self setup];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [self setup];
    }
    return self;
}





- (void)setup
{
    self.backgroundColor = [UIColor whiteColor];
    buttonWidth = self.frame.size.width / 2;
    offset = (self.frame.size.height - buttonWidth) / 2;
    
    
    
    self.hoursScroll = [self scrollForN:24 row:0];
    [self addSubview:self.hoursScroll];

    
    self.minutesScroll = [self scrollForN:60 row:1];
    [self addSubview:self.minutesScroll];
}



- (UIScrollView *)scrollForN:(NSInteger)n row:(NSInteger)row
{
    UIScrollView *scroll = [UIScrollView new];
    scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(row * buttonWidth, 0, buttonWidth, self.frame.size.height)];
    scroll.contentSize = CGSizeMake(buttonWidth, n * buttonWidth + offset * 2);
    scroll.showsHorizontalScrollIndicator = NO;
    scroll.showsVerticalScrollIndicator = NO;
//    scroll.decelerationRate = UIScrollViewDecelerationRateFast;
    scroll.tag = row;
    scroll.delegate = self;
    
    NSMutableArray *arr = [NSMutableArray new];
    
    for (int i = 0; i < n; ++i) {
        UIButton *b = [UIButton buttonWithType:UIButtonTypeCustom];
        b.frame = CGRectMake(0, offset + i * buttonWidth, buttonWidth, buttonWidth);
        
        if (!row) {   //hours
            [b setTitle:[NSString stringWithFormat:@"%i", i] forState:UIControlStateNormal];
        } else {     //minutes
            [b setTitle:[NSString stringWithFormat:@"%02i", i] forState:UIControlStateNormal];
        }

        [b setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        b.titleLabel.font = [UIFont fontWithName:@"Heroic Condensed" size:40];
        [b setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [b addTarget:self action:@selector(buttonSelected:) forControlEvents:UIControlEventTouchDown];
        [arr addObject:b];
    }

    for (UIButton *b in arr) {
        [scroll addSubview:b];
    }
    return scroll;
}


- (void)buttonSelected:(UIButton *)sender
{
    UIScrollView *scrollView = (UIScrollView *)sender.superview;
    [scrollView setContentOffset:CGPointMake(sender.frame.origin.x, sender.frame.origin.y - offset) animated:YES];

    if (!scrollView.tag) {
        _hour = sender.tag;
    } else {
        _minute = sender.tag;
    }
//    NSLog(@"%i:%02i", self.hour, self.minute);
}



- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate) {
        [self scrollViewDidEndDecelerating:scrollView];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    CGFloat modulo = (NSInteger)scrollView.contentOffset.y % (NSInteger)buttonWidth;
    
    if (!scrollView.tag) {
        _hour = (scrollView.contentOffset.y - modulo) / buttonWidth;
    } else {
        _minute = (scrollView.contentOffset.y - modulo) / buttonWidth;
    }
    

    
    
    CGFloat tmp;
    if (modulo < buttonWidth / 2) {
        tmp = scrollView.contentOffset.y - modulo;
    } else
    tmp = scrollView.contentOffset.y + buttonWidth - modulo;
//    NSLog(@"%i:%02i", self.hour, self.minute);
    [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, tmp) animated:YES];
}


- (void)setHour:(NSInteger)hour
{    
    [self.hoursScroll setContentOffset:CGPointMake(0, buttonWidth * hour) animated:YES];
    _hour = hour;
}

- (void)setMinute:(NSInteger)minute
{
    [self.minutesScroll setContentOffset:CGPointMake(0, buttonWidth * minute) animated:YES];
    _minute = minute;
}




@end
