//
//  TimePicker.h
//  TimePickerProject
//
//  Created by Mikhail Baynov on 17/12/14.
//  Copyright (c) 2014 StarDreams. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimePicker : UIView

@property (strong, nonatomic) UIScrollView *hoursScroll;
@property (strong, nonatomic) UIScrollView *minutesScroll;
@property (strong, nonatomic) NSArray *hourButtonsArray;
@property (strong, nonatomic) NSArray *minuteButtonsArray;


@property (nonatomic) NSInteger hour;
@property (nonatomic) NSInteger minute;




@end
